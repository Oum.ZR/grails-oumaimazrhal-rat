<%@ page import="test.User" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
    <style>
    .upload-area {
        width: 70%;
        height: 200px;
        border: 2px solid lightgray;
        border-radius: 3px;
        margin: 0 auto;
        margin-top: 100px;
        text-align: center;
        overflow: auto;
    }

    .upload-area:hover {
        cursor: pointer;
    }

    .upload-area h1 {
        text-align: center;
        font-weight: normal;
        font-family: sans-serif;
        line-height: 50px;
        color: darkslategray;
    }

    #file {
        display: none;
    }

    /* Thumbnail */
    .thumbnail {
        width: 80px;
        height: 80px;
        padding: 2px;
        border: 2px solid lightgray;
        border-radius: 3px;
        float: left;
    }

    .size {
        font-size: 12px;
    }
    </style>
</head>

<body>
<a href="#edit-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="edit-annonce" class="content scaffold-edit" role="main">
    <h1><g:message code="default.edit.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.annonce}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.annonce}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form resource="${this.annonce}" method="PUT">
        <g:hiddenField name="version" value="${this.annonce?.version}"/>
        <fieldset class="form">
            <fieldset class="form">
                <div class="fieldcontain required">
                    <label for="title">Title
                        <span class="required-indicator">*</span>
                    </label><input type="text" name="title" value="${annonce.title}" required="" id="title">
                </div>

                <div class="fieldcontain required">
                    <label for="description">Description
                        <span class="required-indicator">*</span>
                    </label><input type="text" name="description" value="${annonce.description}" required=""
                                   id="description">
                </div>

                <div class="fieldcontain required">
                    <label for="price">Price
                        <span class="required-indicator">*</span>
                    </label><input type="number decimal" name="price" value="${annonce.price}" required="" step="0.01"
                                   min="0.0" id="price">
                </div>

                <div class="fieldcontain" style="display: flex">
                    <label for="illustrations">Illustrations</label>
                    <ul id="picture-container">
                        <g:each in="${annonce.illustrations}" var="illustration">
                            <li><img src="${grailsApplication.config.assets.url + illustration.filename}"/></li>
                        </g:each>
                    </ul>
                </div>

                <div class="container">
                    <input type="file" name="file" id="file">

                    <!-- Drag and Drop container-->
                    <div class="upload-area" id="uploadfile">
                        <h1>Drag and Drop file here<br/>Or<br/>Click to select file</h1>
                    </div>
                </div>


                <div class="fieldcontain required">
                    <label for="author">Author
                        <span class="required-indicator">*</span>
                    </label>
                    <g:select from="${User.list()}" name="author.id" optionKey="id" optionValue="username"/>
                </div>
            </fieldset>
        </fieldset>
        <fieldset class="buttons">
            <input class="save" type="submit"
                   value="${message(code: 'default.button.update.label', default: 'Update')}"/>
        </fieldset>
    </g:form>
</div>
<asset:javascript src="application.js"/>
<script type="text/javascript">
    $(function() {
        // preventing page from redirecting
        $("html").on("dragover", function (e) {
            e.preventDefault();
            e.stopPropagation();
            $("h1").text("Drag here");
        });

        $("html").on("drop", function (e) {
            e.preventDefault();
            e.stopPropagation();
        });

        // Drag enter
        $('.upload-area').on('dragenter', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $("h1").text("Drop");
        });

        // Drag over
        $('.upload-area').on('dragover', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $("h1").text("Drop");
        });

        // Drop
        $('.upload-area').on('drop', function (e) {
            e.stopPropagation();
            e.preventDefault();

            $("h1").text("Upload");

            var file = e.originalEvent.dataTransfer.files;
            var fd = new FormData();

            fd.append('file', file[0]);

            uploadData(fd);
        });

        // Open file selector on div click
        $("#uploadfile").click(function () {
            $("#file").click();
        });

        // file selected
        $("#file").change(function () {
            var fd = new FormData();

            var files = $('#file')[0].files[0];

            fd.append('file', files);

            uploadData(fd);
        });
    });

    // Sending AJAX request and upload file
    function uploadData(formdata) {
        $.ajax({
            url: "${g.createLink(controller:'annonce',action:'uploadIllustration', id: annonce.id)}",
            type: 'post',
            data: formdata,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
                addThumbnail(response);
            }
        });
    }

    // Added thumbnail
    function addThumbnail(data) {
        $("#uploadfile h1").remove();
        $("#uploadfile").append('<h1>Drag and Drop file here<br/>Or<br/>Click to select file</h1>');

        var name = data.name;
        var src = data.src;

        $("#picture-container").append('<li><img src="'+src+'"/></li>');
    }

    // Bytes conversion
    function convertSize(size) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (size == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(size) / Math.log(1024)));
        return Math.round(size / Math.pow(1024, i), 2) + ' ' + sizes[i];
    }

</script>
</body>
</html>
